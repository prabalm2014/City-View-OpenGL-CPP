#include <stdio.h>
#include <GL/glut.h>
#include <math.h>
#include "imageloader.h"

int x,y;
//float r=12; 
int num_segments=1000;
int f=990,g=180,g2,h=90,m=700,n=90,tc = 0;
int i=1,j,o,oo,a,aa,k,l,d,c;
bool stop = false;
int xx1 = 0,yy1 = 0,xx2 = 0,yy2 = 0,xx3 = 0,yy3 = 0,xx4 = 0,yy4 = 0;//for car light
int xa1 = 0,ya1 = 0,xa2 = 0,ya2 = 0,xa3 = 0,ya3 = 0,xa4 = 0,ya4 = 0;
int xb1 = 0,yb1 = 0,xb2 = 0,yb2 = 0,xb3 = 0,yb3 = 0,xb4 = 0,yb4 = 0;
int xc1 = 0,yc1 = 0,xc2 = 0,yc2 = 0,xc3 = 0,yc3 = 0,xc4 = 0,yc4 = 0;

GLuint _textureId,_textureId1, _textureId2, _textureId3, _textureId4, _textureId5, _textureId6, _textureId7, _textureId8, _textureId9, _textureId10, _textureId11;


float ca = 1.0,cb = 1.0,cc = 1.0;//sky
float cd = 0.0,ce = 0.0,cf = 0.5;// building1 roof top,front,side
float cca = 0.0,ccb = 0.0,ccc = 0.7;//building 1 side
float cg = 0.5,ch = 0.8,ci = 0.2; //building 2 front
float cj = 1.0,ck = 1.0,cl = 1.0;//building 2 roof
float cm = 1.0,cn = 1.0,co = 1.0;//building 2 fireplace
float cp = 0.45,cq = 0.71,cr = 0.230;// building 3
float cs = 0.25,ct = 0.0,cu = 0.230;//building 4
float cv = 0.45,cw = 0.51,cx = 0.230;//building 5
float cy = 0.145,cz = 0.51,c0 = 0.230;//building 5 side
float cba = 1.0,cbb = 1.0,cbc = 1.0;//building 6
float cbd = 1.0,cbe = 1.0,cbf = 1.0;//building 6 roof
float cbg = 1.0,cbh = 1.0,cbi =1.0;//road
float cbj = 1.0,cbk = 1.0,ckl = 1.0;//foothpath brick
float cbm = 0.5,cbn = 0.7,cbo = 0.2;//foothpath side
float cbp = 0.90,cbq = 0.130,cbr = 0.130;//car 1
float cbs = 0.70,cbt = 1.230,cbu = 0.280;//car2
float cbv = 1.0,cbw = 1.0, cbx = 1.0;//car3
float cby = 0.89,cbz = 0.230,cb0 = 0.230;//car 4
float ccd = 1.0,cce = 1.0,ccf = 1.0;//building 1 glass
float ccg = 1.0,cch = 1.0,cci = 1.0;//building 2 glass
float ccj = 1.0,cck = 1.0,ccl = 1.0;//building 2 midl,rit glass
float ccm = 1.0,ccn = 1.0,cco = 1.0;//building 3,6 glass
float ccp = 1.0,ccq = 1.0,ccr = 1.0;//building 4 glass
float ccs = 0.20,cct = 0.20,ccu = 0.20;//car 1 ,4glass
float ccv = 0.20,ccw = 0.20,ccx = 0.20;//car 3 glass
float ccy = 1.0,ccz = 1.0,cc0 = 1.0;

void day()
{
	ca = 1.0,cb = 1.0,cc = 1.0;//sky
	cd = 0.0,ce = 0.0,cf = 0.5;// building1 roof top,front,side
	cca = 0.0,ccb = 0.0,ccc = 0.7;//building 1 side
	cg = 0.5,ch = 0.8,ci = 0.2; //building 2 front
	cj = 1.0,ck = 1.0,cl = 1.0;//building 2 roof
	cm = 1.0,cn = 1.0,co = 1.0;//building 2 fireplace
	cp = 0.45,cq = 0.71,cr = 0.230;// building 3
	cs = 0.25,ct = 0.0,cu = 0.230;//building 4
	cv = 0.45,cw = 0.51,cx = 0.230;//building 5
	cy = 0.145,cz = 0.51,c0 = 0.230;//building 5 side
	cba = 1.0,cbb = 1.0,cbc = 1.0;//building 6
	cbd = 1.0,cbe = 1.0,cbf = 1.0;//building 6 roof
	cbg = 1.0,cbh = 1.0,cbi =1.0;//road
	cbj = 1.0,cbk = 1.0,ckl = 1.0;//foothpath brick
	cbm = 0.5,cbn = 0.7,cbo = 0.2;//foothpath side
	cbp = 0.90,cbq = 0.130,cbr = 0.130;//car 1
	cbs = 0.70,cbt = 1.230,cbu = 0.280;//car2
	cbv = 1.0,cbw = 1.0, cbx = 1.0;//car3
	cby = 0.89,cbz = 0.230,cb0 = 0.230;//car 4
	cbv = 1.0,cbw = 1.0, cbx = 1.0;//	cby = 0.89,cbz = 0.230,cb0 = 0.230;//car 4
	ccd = 1.0,cce = 1.0,ccf = 1.0;//building 1 glass
	ccg = 1.0,cch = 1.0,cci = 1.0;//building 2 glass
	ccj = 1.0,cck = 1.0,ccl = 1.0;//building 2 midl,rit glass
	ccm = 1.0,ccn = 1.0,cco = 1.0;//building 3,6 glass
	ccp = 1.0,ccq = 1.0,ccr = 1.0;//building 4 glass
	ccs = 0.20,cct = 0.20,ccu = 0.20;//car 1 ,4glass
	ccv = 0.20,ccw = 0.20,ccx = 0.20;//car 3 glass
	ccy = 1.0,ccz = 1.0,cc0 = 1.0;//water

	xx1=0;xa1=0;xb1=0;xc1=0;
	yy1=0;ya1=0;yb1=0;yc1=0;
	xx2=0;xa2=0;xb2=0;xc2=0;
	yy2=0;ya2=0;yb2=0;yc2=0;
	xx3=0;xa3=0;xb3=0;xc3=0;
	yy3=0;ya3=0;yb3=0;yc3=0;
	xx4=0;xa4=0;xb4=0;xc4=0;
	yy4=0;ya4=0;yb4=0;yc4=0;
}
void night()
{
	ca = 0.0,cb = 0.220,cc = 0.220;//sky night
	cd = 0.0,ce = 0.160,cf = 0.221;// building1 roof top,front,side night
	cca = 0.0,ccb = 0.160,ccc = 0.221;//building 1 side night
	cg = 0.250,ch = 0.240,ci = 0.100; //building 2 front night
	cj =0.200,ck = 0.210,cl = 0.210;//building 2 roof night
	cm = 0.10,cn = 0.30,co = 0.30;//building 2 fireplace night
	cp = 0.20,cq = 0.251,cr = 0.150;// building 3 night
	cs = 0.0,ct = 0.160,cu = 0.221;//building 4 night
	cv = 0.225,cw = 0.251,cx = 0.20;//building 5 night
	cy = 0.15,cz = 0.251,c0 = 0.230;//building 5 side night
	cba = 0.20,cbb = 0.200,cbc = 0.220;//building 6 night
	cbd = 0.190,cbe = 0.220,cbf = 0.200;//building 6 roof night
	cbg = 0.1,cbh = 0.2,cbi = 0.2;//road night
	cbj = 0.10,cbk = 0.30,ckl = 0.20;//foothpath brick night
	cbm = 0.0,cbn = 0.200,cbo = 0.0;//footpath side night
	cbp = 0.120,cbq = 0.230,cbr = 0.130;//car 1 night
	cbs = 0.05,cbt = 0.230,cbu = 0.0;//car2 night
	cbv = 0.255,cbw = 0.255, cbx = 0.255;//car3 night
	cby = 0.229,cbz = 0.220,cb0 = 0.180;//car 4 night
	ccd = 1.0,cce = 1.0,ccf = 0.2;//building 1 glass night
	ccg = 0.50,cch = 1.0,cci = 0.10;//building 2 glass
	ccj = 1.0,cck = 1.0,ccl = 0.50;//building 2 midl,rit glass night
	ccm = 1.0,ccn = 1.0,cco = 0.0;//building 3 night
	ccp = 1.0,ccq = 0.0,ccr = 1.0;//building 4 glass night
	ccs = 0.0,cct = 1.20,ccu = 0.200;//car 1 ,4 glass night
	ccv = 1.0,ccw = 1.0,ccx = 1.0;//car 3 glass night
	ccy = 0.0,ccz = 0.220,cc0 = 0.220;//water night

	xx1=180;xa1=180;xb1=50;xc1=50;
	yy1=160;ya1=160;yb1=160;yc1=160;
	xx2=260;xa2=260;xb2=40;xc2=15;
	yy2=155;ya2=155;yb2=155;yc2=155;
	xx3=260;xa3=260;xb3=40;xc3=15;
	yy3=180;ya3=180;yb3=180;yc3=180;
	xx4=180;xa4=180;xb4=50;xc4=50;
	yy4=165;ya4=165;yb4=165;yc4=165;
}

GLuint loadTexture(Image* image) {
	GLuint textureId;
	glGenTextures(1, &textureId); //Make room for our texture
	glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
	//Map the image to the texture
	glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D
		0,                            //0 for now
		GL_RGB,                       //Format OpenGL uses for image
		image->width, image->height,  //Width and height
		0,                            //The border of the image
		GL_RGB, //GL_RGB, because pixels are stored in RGB format
		GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
		//as unsigned numbers
		image->pixels);               //The actual pixel data
	return textureId; //Returns the id of the texture
}
void torch()
{
	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex2i(i+(k+xx1), yy1);
	glVertex2i(i+(k+xx2), yy2);
	glVertex2i(i+(k+xx3), yy3);
	glVertex2i(i+(k+xx4), yy4);
	glEnd();

	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex2i(i+(f+xa1), ya1);
	glVertex2i(i+(f+xa2), ya2);
	glVertex2i(i+(f+xa3), ya3);
	glVertex2i(i+(f+xa4), ya4);
	glEnd();

	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex2i((xb1+g)-i, (yb1+h));
	glVertex2i((-xb2+g)-i, (yb2+h));
	glVertex2i((-xb3+g)-i, (yb3+h));
	glVertex2i((xb4+g)-i, (yb4+h));
	glEnd();

	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex2i((xc1+m)-i, (yc1+n));
	glVertex2i((-xc2+m)-i, (yc2+n));
	glVertex2i((-xc3+m)-i, (yc3+n));
	glVertex2i((xc4+m)-i, (yc4+n));
	glEnd();
}
void initialize() {
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	gluPerspective(1.0, 1.00, 1.0, 1.0);
	Image* image = loadBMP("sk.bmp");
	Image* image1 = loadBMP("road.bmp");
	Image* image2 = loadBMP("fbrick.bmp");
	Image* image3 = loadBMP("roofs.bmp");
	Image* image4 = loadBMP("fire.bmp");
	Image* image5 = loadBMP("win.bmp");
	Image* image6 = loadBMP("door.bmp");
	Image* image7 = loadBMP("glass.bmp");
	Image* image8 = loadBMP("wins.bmp");
	Image* image9 = loadBMP("brick.bmp");
	Image* image10 = loadBMP("bdoor.bmp");
	Image* image11 = loadBMP("water.bmp");
	_textureId = loadTexture(image);
	_textureId1 = loadTexture(image1);
	_textureId2 = loadTexture(image2);
	_textureId3 = loadTexture(image3);
	_textureId4 = loadTexture(image4);
	_textureId5 = loadTexture(image5);
	_textureId6 = loadTexture(image6);
	_textureId7 = loadTexture(image7);
	_textureId8 = loadTexture(image8);
	_textureId9 = loadTexture(image9);
	_textureId10 = loadTexture(image10);
	_textureId11 = loadTexture(image11);
	delete image;
}
void staticDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glPointSize(4.0);

	//sky
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ca,cb,cc);
	glBegin(GL_POLYGON);

	glTexCoord2f(0.0, 0.0);
	glVertex2i(0, 340);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(0, 690);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1350, 690);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1350, 340);

	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//rooftop of biulding 1
	glColor3f(cd,ce,cf);
	glBegin(GL_POLYGON);
	glVertex2i(0, 580);
	glVertex2i(5, 600);
	glVertex2i(120, 600);
	glVertex2i(120, 580);
	glEnd();

	//End of roof 1
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES);
	glVertex2i(0, 580);
	glVertex2i(120, 580);
	glEnd();

	//Front of biulding 1
	glColor3f(cd,ce,cf);
	glBegin(GL_POLYGON);
	glVertex2i(0, 340);
	glVertex2i(120, 340);
	glVertex2i(120, 580);
	glVertex2i(0, 580);
	glEnd();


	//biulding 1 window

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId7);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(10, 570);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(50, 570);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(50, 530);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(10, 530);
	glEnd();


	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(65, 570);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(105, 570);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(105, 530);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(65, 530);
	glEnd();

	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(10, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(50, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(50, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(10, 480);
	glEnd();


	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(65, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(105, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(105, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(65, 480);
	glEnd();

	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(10, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(50, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(50, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(10, 480);
	glEnd();


	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(65, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(105, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(105, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(65, 480);
	glEnd();

	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(10, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(50, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(50, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(10, 430);
	glEnd();


	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(65, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(105, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(105, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(65, 430);
	glEnd();

	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(10, 420);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(50, 420);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(50, 380);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(10, 380);
	glEnd();


	glColor3f(ccd,cce,ccf);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(65, 420);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(105, 420);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(105, 380);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(65, 380);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//Side of biulding 1
	glColor3f(cca,ccb,ccc);
	glBegin(GL_POLYGON);
	glVertex2i(120, 600);
	glVertex2i(135, 580);
	glVertex2i(135, 345);
	glVertex2i(120, 340);
	glEnd();

	//building 2 front
	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(190, 550);
	glVertex2i(380, 550);
	glVertex2i(380, 340);
	glVertex2i(190, 340);
	glEnd();

	//building 2 windows

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId5);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(210, 540);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(250, 540);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(250, 500);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(210, 500);
	glEnd();


	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(270, 540);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(310, 540);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(310, 500);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(270, 500);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(325, 540);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(365, 540);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(365, 500);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(325, 500);
	glEnd();

	//////////////

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(210, 480);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(250, 480);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(250, 440);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(210, 440);
	glEnd();


	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(270, 480);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(310, 480);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(310, 440);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(270, 440);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(325, 480);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(365, 480);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(365, 440);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(325, 440);
	glEnd();

	///////////

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(210, 380);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(250, 380);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(250, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(210, 420);
	glEnd();


	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(270, 380);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(310, 380);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(310, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(270, 420);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(325, 380);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(365, 380);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(365, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(325, 420);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	// building 2 roof
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId3);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cj,ck,cl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(190, 620);

	glTexCoord2f(5.0, 0.0);
	glVertex2i(380, 620);

	glTexCoord2f(5.0, 1.0);
	glVertex2i(415, 550);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(155, 550);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 2 fire place

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cm,cn,co);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(240, 650);

	glTexCoord2f(2.0, 0.0);
	glVertex2i(210, 650);

	glTexCoord2f(2.0, 1.0);
	glVertex2i(210, 610);

	glTexCoord2f(2.0, 0.0);
	glVertex2i(240, 610);
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 2 middle
	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(380, 480);
	glVertex2i(500, 480);
	glVertex2i(500, 340);
	glVertex2i(380, 340);
	glEnd();

	//building 2 middle window

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId5);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(390, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(430, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(430, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(390, 430);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(450, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(490, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(490, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(450, 430);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 2 middle door 

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId6);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(420, 420);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(460, 420);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(460, 350);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(420, 350);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 2 middle rl
	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(400, 480);
	glVertex2i(420, 480);
	glVertex2i(420, 490);
	glVertex2i(400, 490);
	glEnd();

	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(430, 480);
	glVertex2i(450, 480);
	glVertex2i(450, 490);
	glVertex2i(430, 490);
	glEnd();

	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(460, 480);
	glVertex2i(480, 480);
	glVertex2i(480, 490);
	glVertex2i(460, 490);
	glEnd();


	//building 2 right
	glColor3f(cg,ch,ci);
	glBegin(GL_POLYGON);
	glVertex2i(500, 530);
	glVertex2i(650, 530);
	glVertex2i(650, 340);
	glVertex2i(500, 340);
	glEnd();

	//building 2 right window

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId5);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(520, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(560, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(560, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(520, 480);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(580, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(620, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(620, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(580, 480);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(520, 460);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(560, 460);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(560, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(520, 420);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(580, 460);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(620, 460);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(620, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(580, 420);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(520, 400);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(560, 400);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(560, 360);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(520, 360);
	glEnd();

	glColor3f(ccj,cck,ccl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(580, 400);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(620, 400);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(620, 360);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(580, 360);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	// building 2 right roof
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId3);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cj,ck,cl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(470, 530);

	glTexCoord2f(5.0, 0.0);
	glVertex2i(680, 530);

	glTexCoord2f(5.0, 1.0);
	glVertex2i(650, 590);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(500, 590);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);


	//building 2 fire place right
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId4);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cm,cn,co);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(630, 620);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(600, 620);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(600, 580);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(630, 580);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 4

	glColor3f(cs,ct,cu);
	glBegin(GL_POLYGON);
	glVertex2i(800, 630);
	glVertex2i(960, 630);
	glVertex2i(960, 300);
	glVertex2i(800, 300);
	glEnd();

	//building 4 window

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId8);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(810, 620);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(850, 620);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(850, 580);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(810, 580);

	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(860, 620);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(900, 620);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(900, 580);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(860, 580);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(910, 620);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(950, 620);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(950, 580);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(910, 580);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(810, 570);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(850, 570);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(850, 530);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(810, 530);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(860, 570);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(900, 570);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(900, 530);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(860, 530);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(910, 570);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(950, 570);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(950, 530);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(910, 530);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(860, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(900, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(900, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(860, 480);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(910, 520);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(950, 520);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(950, 480);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(910, 480);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(860, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(900, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(900, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(860, 430);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(910, 470);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(950, 470);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(950, 430);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(910, 430);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(860, 420);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(900, 420);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(900, 380);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(860, 380);
	glEnd();

	glColor3f(ccp,ccq,ccr);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(910, 420);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(950, 420);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(950, 380);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(910, 380);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);


	//building 4 side

	glColor3f(0.25, 0.0, 0.130);
	glBegin(GL_POLYGON);
	glVertex2i(960, 630);
	glVertex2i(980, 620);
	glVertex2i(980, 570);
	glVertex2i(960, 570);
	glEnd();

	//building 3
	glColor3f(cp,cq,cr);
	glBegin(GL_POLYGON);
	glVertex2i(700, 550);
	glVertex2i(850, 550);
	glVertex2i(850, 300);
	glVertex2i(700, 300);
	glEnd();

	//building 3 window

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId8);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);

	glVertex2i(720, 535);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(760, 535);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(760, 495);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(720, 495);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(790, 535);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(830, 535);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(830, 495);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(790, 495);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(720, 475);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(760, 475);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(760, 435);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(720, 435);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(790, 475);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(830, 475);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(830, 435);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(790, 435);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(720, 415);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(760, 415);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(760, 375);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(720, 375);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(790, 415);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(830, 415);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(830, 375);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(790, 375);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 5

	glColor3f(cv,cw,cx);
	glBegin(GL_POLYGON);
	glVertex2i(930, 570);
	glVertex2i(1050, 570);
	glVertex2i(1050, 300);
	glVertex2i(930, 300);
	glEnd();

	//building 5 side 

	glColor3f(cy,cz,c0);
	glBegin(GL_POLYGON);
	glVertex2i(1050, 570);
	glVertex2i(1070, 560);
	glVertex2i(1070, 300);
	glVertex2i(1050, 300);
	glEnd();


	//building 5 window 

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId5);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(940, 560);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(980, 560);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(980, 520);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(940, 520);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1000, 560);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1040, 560);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1040, 520);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1000, 520);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(940, 510);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(980, 510);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(980, 470);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(940, 470);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1000, 510);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1040, 510);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1040, 470);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1000, 470);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(940, 460);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(980, 460);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(980, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(940, 420);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1000, 460);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1040, 460);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1040, 420);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1000, 420);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(940, 410);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(980, 410);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(980, 370);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(940, 370);
	glEnd();

	glColor3f(ccg,cch,cci);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1000, 410);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1040, 410);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1040, 370);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1000, 370);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 6

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId9);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cba,cbb,cbc);
	glBegin(GL_POLYGON);

	glTexCoord2f(0.0, 0.0);
	glVertex2i(1120, 570);

	glTexCoord2f(3.0, 0.0);
	glVertex2i(1310, 570);

	glTexCoord2f(3.0, 2.0);
	glVertex2i(1310, 300);

	glTexCoord2f(0.0, 2.0);
	glVertex2i(1120, 300);
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);


	// building 6 window and door

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId8);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1130, 560);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1170, 560);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1170, 520);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1130, 520);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1195, 560);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1235, 560);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1235, 520);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1195, 520);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1258, 560);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1298, 560);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1298, 520);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1258, 520);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1130, 500);
	
	glTexCoord2f(1.0, 0.0);
	glVertex2i(1170, 500);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1170, 460);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1130, 460);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1195, 500);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1235, 500);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1235, 460);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1195, 460);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1258, 500);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1298, 500);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1298, 460);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1258, 460);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1130, 440);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1170, 440);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1170, 400);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1130, 400);
	glEnd();


	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1258, 440);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1298, 440);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1298, 400);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1258, 400);
	glEnd();

	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1130, 390);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1170, 390);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1170, 350);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1130, 350);
	glEnd();


	glColor3f(ccm,ccn,cco);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1258, 390);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1298, 390);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1298, 350);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1258, 350);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//door

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId10);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_POLYGON);

	glTexCoord2f(0.0, 0.0);
	glVertex2i(1180, 450);

	glTexCoord2f(1.0, 0.0);
	glVertex2i(1250, 450);

	glTexCoord2f(1.0, 1.0);
	glVertex2i(1250, 345);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(1180, 345);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//building 6 roof

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId3);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cbd,cbe,cbf);
	glBegin(GL_TRIANGLES);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(1100, 570);

	glTexCoord2f(3.0, 2.0);
	glVertex2i(1330, 570);

	glTexCoord2f(3.0, 0.0);
	glVertex2i(1215, 680);

	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);



	//foothpath 1

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId2);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cbj,cbk,ckl);
	glBegin(GL_POLYGON);

	glTexCoord2f(0.0, 0.0);
	glVertex2i(0, 300);

	glTexCoord2f(30.0, 0.0);
	glVertex2i(1350, 300);

	glTexCoord2f(30.0, 1.0);
	glVertex2i(1350, 340);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(0, 340);
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//footpath side 1
	glColor3f(cbm,cbn,cbo);
	glBegin(GL_POLYGON);
	glVertex2i(0, 300);
	glVertex2i(1350, 300);
	glVertex2i(1350, 285);
	glVertex2i(0, 285);
	glEnd();


	//foothpath 2
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId2);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cbj,cbk,ckl);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(0, 125);

	glTexCoord2f(30.0, 0.0);
	glVertex2i(1350, 125);

	glTexCoord2f(30.0, 1.0);
	glVertex2i(1350, 80);

	glTexCoord2f(0.0, 1.0);
	glVertex2i(0, 80);
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//footpath side 2
	glColor3f(cbm,cbn,cbo);
	glBegin(GL_POLYGON);
	glVertex2i(0, 82);
	glVertex2i(1350, 82);
	glVertex2i(1350, 67);
	glVertex2i(0, 67);
	glEnd();

	//water

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId11);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(ccy,ccz,cc0);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(0, 67);

	glTexCoord2f(0.0, 5.0);
	glVertex2i(1350, 67);

	glTexCoord2f(5.0, 5.0);
	glVertex2i(1350, 0);

	glTexCoord2f(5.0, 0.0);
	glVertex2i(0, 0);
	glEnd();
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//road

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textureId1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glColor3f(cbg,cbh,cbi);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2i(0, 125);

	glTexCoord2f(0.0, 5.0);
	glVertex2i(1350, 125);

	glTexCoord2f(5.0, 5.0);
	glVertex2i(1350, 285);

	glTexCoord2f(5.0, 0.0);
	glVertex2i(0, 285);
	glEnd();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//road line
	for(x=0; x<1340; x=x+45)
	{
		glColor3f(1.0, 1.2, 1.2);
		glBegin(GL_POLYGON);
		glVertex2i(x+0, 198);
		glVertex2i(x+35, 198);
		glVertex2i(x+35, 207);
		glVertex2i(x+0, 207);
		glEnd();
	}

	//car 1
	glColor3f(cbp,cbq,cbr);
	glBegin(GL_POLYGON);
	glVertex2i(i+(g2+50), 145);
	glVertex2i(i+(g2+180), 145);
	glVertex2i(i+(g2+180), 170);
	glVertex2i(i+(g2+150), 180);
	glVertex2i(i+(g2+140), 197);
	glVertex2i(i+(g2+90), 197);
	glVertex2i(i+(g2+90), 184);
	glVertex2i(i+(g2+60), 175);
	glVertex2i(i+(g2+50), 170);
	glEnd();

	// car 1 glass

	glColor3f(ccs,cct,ccu);
	glBegin(GL_POLYGON);
	glVertex2i(i+(g2+76), 175);
	glVertex2i(i+(g2+137), 175);
	glVertex2i(i+(g2+150), 175);
	glVertex2i(i+(g2+137), 196);
	glVertex2i(i+(g2+130), 196);
	glVertex2i(i+(g2+90), 196);
	glEnd();

	//door 1

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES); 
	glVertex2i(i+(g2+93), 195);
	glVertex2i(i+(g2+93), 175);
	glVertex2i(i+(g2+93), 175);
	glVertex2i(i+(g2+85), 170);
	glVertex2i(i+(g2+85), 170);
	glVertex2i(i+(g2+95), 150);
	glVertex2i(i+(g2+95), 150);
	glVertex2i(i+(g2+135), 150);
	glVertex2i(i+(g2+135), 150);
	glVertex2i(i+(g2+135), 195);
	glVertex2i(i+(g2+135), 195);
	glVertex2i(i+(g2+93), 195);

	glVertex2i(i+(g2+90), 195);
	glVertex2i(i+(g2+75), 175);

	glVertex2i(i+(g2+135), 195);
	glVertex2i(i+(g2+150), 175);
	glVertex2i(i+(g2+150), 175);
	glVertex2i(i+(g2+135), 150);

	glVertex2i(i+(g2+75), 175);
	glVertex2i(i+(g2+150), 175);
	glEnd();

	// tyre 1.1

	float theta = 2 * 3.1415926 / float(num_segments); 
	float tangetial_factor = tanf(theta);//calculate the tangential factor 

	float radial_factor = cosf(theta);//calculate the radial factor 
	
	float x = 12;//start at angle = 0 

	float y = 0; 
    
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(oo +(x + 70+i), y + 145);//output vertex 1
        
		//calculate the tangential vector 
		//remember, the radial vector is (x, y) 
		//to get the tangential vector we flip those coordinates and negate one of them 

		float tx = -y; 
		float ty = x; 
        
		//add the tangential vector 

		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
        
		//correct using the radial factor 

		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd();

	// tyre 1.2
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(oo + (x + 160+i), y + 145);//output vertex 2
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd(); 

	//car 2
	glColor3f(cbs,cbt,cbu);
	glBegin(GL_POLYGON);
	glVertex2i(i+(f+50), 145);
	glVertex2i(i+(f+180), 145);
	glVertex2i(i+(f+180), 170);
	glVertex2i(i+(f+150), 180);
	glVertex2i(i+(f+140), 197);
	glVertex2i(i+(f+90), 197);
	glVertex2i(i+(f+90), 184);
	glVertex2i(i+(f+60), 175);
	glVertex2i(i+(f+50), 170);
	glEnd();

	// car 2 glass

	glColor3f(ccv,ccw,ccx);
	glBegin(GL_POLYGON);
	glVertex2i(i+(f+76), 175);
	glVertex2i(i+(f+137), 175);
	glVertex2i(i+(f+150), 175);
	glVertex2i(i+(f+137), 196);
	glVertex2i(i+(f+130), 196);
	glVertex2i(i+(f+90), 196);
	glEnd();

	//door 2

	glColor3f(0.0, 0.0, 1.0);
	glBegin(GL_LINES); 
	glVertex2i(i+(f+93), 195);
	glVertex2i(i+(f+93), 175);
	glVertex2i(i+(f+93), 175);
	glVertex2i(i+(f+85), 170);
	glVertex2i(i+(f+85), 170);
	glVertex2i(i+(f+95), 150);
	glVertex2i(i+(f+95), 150);
	glVertex2i(i+(f+135), 150);
	glVertex2i(i+(f+135), 150);
	glVertex2i(i+(f+135), 195);
	glVertex2i(i+(f+135), 195);
	glVertex2i(i+(f+93), 195);

	glVertex2i(i+(f+90), 195);
	glVertex2i(i+(f+75), 175);

	glVertex2i(i+(f+135), 195);
	glVertex2i(i+(f+150), 175);
	glVertex2i(i+(f+150), 175);
	glVertex2i(i+(f+135), 150);

	glVertex2i(i+(f+75), 175);
	glVertex2i(i+(f+150), 175);
	glEnd();

	// tyre 2.1
    
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(o + (x + 1060 +i), (y + 145));
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd();

	// tyre 2.2
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(o+(x + 1150) +i, y + 145);//output vertex 2
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd(); 

	//car opposite
	//car 3 
	glColor3f(cbv,cbw,cbx);
	glBegin(GL_POLYGON);
	glVertex2i((50+g)-i, (145+h));
	glVertex2i((180+g)-i, (145+h));
	glVertex2i((180+g)-i, (170+h));
	glVertex2i((150+g)-i, (180+h));
	glVertex2i((140+g)-i, (197+h));
	glVertex2i((90+g)-i, (197+h));
	glVertex2i((90+g)-i, (184+h));
	glVertex2i((60+g)-i, (175+h));
	glVertex2i((50+g)-i, (170+h));
	glEnd();

	// car 3 glass
	glColor3f(ccv,ccw,ccx);
	glBegin(GL_POLYGON);
	glVertex2i((76+g)-i, (175+h));
	glVertex2i((137+g)-i, (175+h));
	glVertex2i((150+g)-i, (175+h));
	glVertex2i((137+g)-i, (196+h));
	glVertex2i((130+g)-i, (196+h));
	glVertex2i((90+g)-i, (196+h));
	glEnd();

	//door 3

	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES); 
	
	glVertex2i((93+g)-i, (195+h));
	glVertex2i((93+g)-i, (175+h));
	glVertex2i((93+g)-i, (175+h));
	glVertex2i((93+g)-i, (150+h));
	glVertex2i((93+g)-i, (150+h));
	glVertex2i((135+g)-i, (150+h));
	glVertex2i((135+g)-i, (150+h));
	glVertex2i((135+g)-i, (195+h));
	glVertex2i((135+g)-i, (195+h));
	glVertex2i((93+g)-i, (195+h));

	glVertex2i((135+g)-i, (150+h));
	glVertex2i((140+g)-i, (150+h));
	glVertex2i((140+g)-i, (150+h));
	glVertex2i((150+g)-i, (165+h));

	glVertex2i((150+g)-i, (165+h));
	glVertex2i((140+g)-i, (175+h));

	glVertex2i((90+g)-i, (195+h));
	glVertex2i((75+g)-i, (175+h));

	glVertex2i((93+g)-i, (195+h));
	glVertex2i((75+g)-i, (175+h));
	glVertex2i((75+g)-i, (175+h));
	glVertex2i((93+g)-i, (150+h));

	glVertex2i((75+g)-i, (175+h));
	glVertex2i((150+g)-i, (175+h));
	glEnd();

	// tyre 3.1
    
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(a + (x + 250-i), y + 235);
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd();

	// tyre 3.2
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(a + (x + 340 -i), y + 235);//output vertex 2
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd(); 

	//car 4
	glColor3f(cby,cbz,cb0);
	glBegin(GL_POLYGON);
	glVertex2i((50+m)-i, (145+n));
	glVertex2i((180+m)-i, (145+n));
	glVertex2i((180+m)-i, (170+n));
	glVertex2i((150+m)-i, (180+n));
	glVertex2i((140+m)-i, (197+n));
	glVertex2i((90+m)-i, (197+n));
	glVertex2i((90+m)-i, (184+n));
	glVertex2i((60+m)-i, (175+n));
	glVertex2i((50+m)-i, (170+n));
	glEnd();

	// car 4 glass

	glColor3f(ccs,cct,ccu);
	glBegin(GL_POLYGON);
	glVertex2i((76+m)-i, (175+n));
	glVertex2i((137+m)-i, (175+n));
	glVertex2i((150+m)-i, (175+n));
	glVertex2i((137+m)-i, (196+n));
	glVertex2i((130+m)-i, (196+n));
	glVertex2i((90+m)-i, (196+n));
	glEnd();

	//door 4

	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINES); 
	glVertex2i((93+m)-i, (195+n));
	glVertex2i((93+m)-i, (175+n));
	glVertex2i((93+m)-i, (175+n));
	glVertex2i((93+m)-i, (150+n));
	glVertex2i((93+m)-i, (150+n));
	glVertex2i((135+m)-i, (150+n));
	glVertex2i((135+m)-i, (150+n));
	glVertex2i((135+m)-i, (195+n));
	glVertex2i((135+m)-i, (195+n));
	glVertex2i((93+m)-i, (195+n));

	glVertex2i((135+m)-i, (150+n));
	glVertex2i((140+m)-i, (150+n));
	glVertex2i((140+m)-i, (150+n));
	glVertex2i((150+m)-i, (165+n));

	glVertex2i((150+m)-i, (165+n));
	glVertex2i((140+m)-i, (175+n));

	glVertex2i((90+m)-i, (195+n));
	glVertex2i((75+m)-i, (175+n));

	glVertex2i((93+m)-i, (195+n));
	glVertex2i((75+m)-i, (175+n));
	glVertex2i((75+m)-i, (175+n));
	glVertex2i((93+m)-i, (150+n));

	glVertex2i((75+m)-i, (175+n));
	glVertex2i((150+m)-i, (175+n));
	glEnd();

	// tyre 4.1
    
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(aa +(x + 770 - i), y + 235);
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd();

	// tyre 4.2
	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_POLYGON); 
	for(int k = 0; k < num_segments; k++) 
	{ 
		glVertex2f(aa +(x + 860 - i), y + 235);//output vertex 2
		float tx = -y; 
		float ty = x; 
		x += tx * tangetial_factor; 
		y += ty * tangetial_factor; 
		x *= radial_factor; 
		y *= radial_factor; 
	} 
	glEnd();

	torch();

	glFlush();
}
void update(int value) {
	if (stop){
		glutPostRedisplay(); //Tell GLUT that the display has changed

	}
	else{
		if (i >= 0)
		{
			i += 2;
		}
		if (i >= 2050)
		{
			i = 0;
			j = 0;
			f = -800;
			o = -1790;
			g2 = -300;
			oo = -300;
			k= -300;
		}
		if (i <= 0)
		{
			g = 1400;
			m = 1800;
			a = 1220;
			aa = 1100;
		}
		glVertex2i(i, j);

		glutPostRedisplay(); //Tell GLUT that the display has changed
		//Tell GLUT to call update again in 25 milliseconds
		glutTimerFunc(15, update, 0);
	}
}
void handleKeypress(unsigned char key, int x, int y) 
{
	switch (key) {
	case 's':
		stop = true;
		break;
	case 'f':
		stop = false;
		update(0);
		break;
	case 'n':
		night();
		torch();
		break;
	case 'd':
		day();
		break;
	}
}
void myInit (void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(4.0);
	//glMatrixMode(GL_MODELVIEW | GL_PROJECTION | GL_LINES);
	glMatrixMode(GL_LINES);
	glLoadIdentity();
	gluOrtho2D(0.0, 1350.0, 0.0, 690.0);
}
void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (1350, 690);
	glutInitWindowPosition (0, 0);
	glutCreateWindow ("City Life");
	initialize();
	glutDisplayFunc(staticDisplay);
	glutTimerFunc(15, update, 0);
	glutKeyboardFunc(handleKeypress);
	myInit ();
	glutMainLoop();
}
